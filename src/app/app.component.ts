import { Component } from '@angular/core';
import { removeDebugNodeFromIndex } from '@angular/core/src/debug/debug_node';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
   public title = "Angular 7";
   public myclass = "testclass";
   public haserror=true;
   public mystyles={
     color:"red",
     fontStyle:"italic",
     backgroundcolor:"blue"
   }
   public iserror = "Sunil";
   public names = ["Sunil","Sunny","Charan","Jeevan","Sumanth","Naveen"];
  }

  

